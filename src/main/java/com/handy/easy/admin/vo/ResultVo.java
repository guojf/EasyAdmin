package com.handy.easy.admin.vo;

import com.handy.easy.admin.constants.enums.ResultEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回对象
 *
 * @author handy
 */
@Data
@ApiModel(value = "返回对象")
public class ResultVo<T> implements Serializable {
    @ApiModelProperty(value = "返回编码")
    private String code;
    @ApiModelProperty(value = "返回消息")
    private String msg;
    @ApiModelProperty(value = "返回数据")
    private T data;

    private ResultVo(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    private ResultVo(ResultEnum resultEnum, String msg) {
        this.code = resultEnum.getCode();
        this.msg = msg;
    }

    private ResultVo(ResultEnum resultEnum, T data) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.data = data;
    }

    private ResultVo(ResultEnum resultEnum, String msg, T data) {
        this.code = resultEnum.getCode();
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResultVo<T> success() {
        return new ResultVo<>(ResultEnum.SUCCESS);
    }

    public static <T> ResultVo<T> success(T data) {
        return new ResultVo<>(ResultEnum.SUCCESS, data);
    }

    public static <T> ResultVo<T> success(String msg) {
        return new ResultVo<>(ResultEnum.SUCCESS, msg);
    }

    public static <T> ResultVo<T> success(String msg, T data) {
        return new ResultVo<>(ResultEnum.SUCCESS, msg, data);
    }

    public static <T> ResultVo<T> failure(ResultEnum resultEnum) {
        return new ResultVo<>(resultEnum);
    }

    public static <T> ResultVo<T> failure(String msg) {
        return new ResultVo<>(ResultEnum.FAILURE, msg);
    }

    public static <T> ResultVo<T> failure(ResultEnum resultEnum, String msg) {
        return new ResultVo<>(resultEnum, msg);
    }

    public static <T> ResultVo<T> failure(ResultEnum resultEnum, T data) {
        return new ResultVo<>(resultEnum, data);
    }

}