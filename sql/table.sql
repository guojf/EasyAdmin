CREATE TABLE IF NOT EXISTS `sys_user_account`
(
    `id`           bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `user_id`      bigint(20)  NOT NULL COMMENT '用户ID',
    `source`       varchar(64) NOT NULL COMMENT '来源',
    `uuid`         varchar(64) NULL     DEFAULT NULL COMMENT '一般为第三方平台的用户ID',
    `username`     varchar(64) NULL     DEFAULT NULL COMMENT '用户名',
    `email`        varchar(64) NULL     DEFAULT NULL COMMENT '邮箱',
    `password`     varchar(32) NULL     DEFAULT NULL COMMENT '密码',
    `deleted_flag` bit(1)      NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20)  NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime    NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20)  NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime    NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '用户账号'
  ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `sys_role_menu`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `role_id`      bigint(20) NOT NULL COMMENT '角色ID',
    `menu_id`      bigint(20) NOT NULL COMMENT '菜单ID',
    `deleted_flag` bit(1)     NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20) NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime   NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20) NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime   NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色菜单'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_menu`
(
    `id`           bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `category`     int(2)       NOT NULL COMMENT '类别(1:目录,2:菜单,3:按钮)',
    `parent_id`    bigint(20)   NOT NULL COMMENT '父级id',
    `title`        varchar(255) NOT NULL COMMENT '标题',
    `icon`         varchar(255) NULL     DEFAULT NULL COMMENT '图标',
    `href`         varchar(255) NULL     DEFAULT NULL COMMENT '访问路径',
    `target`       varchar(255) NULL     DEFAULT '_self' COMMENT '打开方式',
    `sort`         int(11)      NULL     DEFAULT NULL COMMENT '排序',
    `deleted_flag` bit(1)       NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime     NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime     NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '菜单资源'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_permission`
(
    `id`           bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `permission`   varchar(255) NOT NULL COMMENT '权限',
    `url`          varchar(255) NOT NULL COMMENT '访问路径',
    `name`         varchar(255) NOT NULL COMMENT '名称',
    `deleted_flag` bit(1)       NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime     NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime     NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '权限资源'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_role_permission`
(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `role_id`       bigint(20) NOT NULL COMMENT '角色ID',
    `permission_id` bigint(20) NOT NULL COMMENT '权限ID',
    `deleted_flag`  bit(1)     NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`    bigint(20) NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`   datetime   NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`    bigint(20) NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`   datetime   NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色权限'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_role`
(
    `id`           bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`         varchar(64)  NOT NULL COMMENT '中文名称',
    `permission`   varchar(255) NULL COMMENT '权限',
    `sort`         int(11)      NULL     DEFAULT NULL COMMENT '排序',
    `note`         varchar(100) NULL     DEFAULT NULL COMMENT '描述',
    `deleted_flag` bit(1)       NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime     NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime     NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '角色'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_user_role`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `user_id`      bigint(20) NOT NULL COMMENT '用户ID',
    `role_id`      bigint(20) NOT NULL COMMENT '角色',
    `deleted_flag` bit(1)     NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20) NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime   NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20) NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime   NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '账户角色'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `sys_user`
(
    `id`           bigint(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `code`         varchar(64)  NOT NULL COMMENT '用户编码',
    `name`         varchar(64)  NOT NULL COMMENT '昵称',
    `sex`          int(1)       NULL     DEFAULT NULL COMMENT '性别-1:男;0:女',
    `birth_data`   datetime     NULL     DEFAULT NULL COMMENT '出生日期',
    `logo`         varchar(500) NULL     DEFAULT NULL COMMENT '头像',
    `email`        varchar(128) NULL     DEFAULT NULL COMMENT '电子邮箱',
    `lock_flag`    bit(1)       NOT NULL DEFAULT b'0' COMMENT '是否锁定（0:未锁定;1:已锁定）',
    `note`         varchar(256) NULL     DEFAULT NULL COMMENT '描述',
    `deleted_flag` bit(1)       NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
    `create_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '创建者',
    `create_time`  datetime     NULL     DEFAULT NULL COMMENT '创建时间',
    `update_uid`   bigint(20)   NULL     DEFAULT NULL COMMENT '最后修改者',
    `update_time`  datetime     NULL     DEFAULT NULL COMMENT '最后修改时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '系统用户'
  ROW_FORMAT = DYNAMIC;