package com.handy.easy.admin.util;

import com.handy.easy.admin.config.exception.EasyAdminRuntimeException;
import com.handy.easy.admin.constants.enums.ResultEnum;

/**
 * 断言
 *
 * @author handy
 */
public class AssertUtil {

    /**
     * 为true 抛出异常
     *
     * @param expression 入参
     * @param resultEnum 异常
     */
    public static void isTrue(boolean expression, ResultEnum resultEnum) {
        if (expression) {
            throw new EasyAdminRuntimeException(resultEnum);
        }
    }

    /**
     * 为true 抛出异常
     *
     * @param expression 入参
     */
    public static void isTrue(boolean expression) {
        isTrue(expression, ResultEnum.FAILURE);
    }

    /**
     * 不为true 抛出异常
     *
     * @param expression 入参
     * @param resultEnum 异常
     */
    public static void isNotTrue(boolean expression, ResultEnum resultEnum) {
        if (!expression) {
            throw new EasyAdminRuntimeException(resultEnum);
        }
    }

    /**
     * 不为true 抛出异常
     *
     * @param expression 入参
     */
    public static void isNotTrue(boolean expression) {
        isNotTrue(expression, ResultEnum.FAILURE);
    }

    /**
     * 为null 抛出异常
     *
     * @param object     对象
     * @param resultEnum 异常
     */
    public static void isNull(Object object, ResultEnum resultEnum) {
        if (object == null) {
            throw new EasyAdminRuntimeException(resultEnum);
        }
    }

    /**
     * 为null 抛出异常
     *
     * @param object 对象
     */
    public static void isNull(Object object) {
        isNull(object, ResultEnum.FAILURE);
    }

    /**
     * 不为null 抛出异常
     *
     * @param object     对象
     * @param resultEnum 异常
     */
    public static void isNotNull(Object object, ResultEnum resultEnum) {
        if (object != null) {
            throw new EasyAdminRuntimeException(resultEnum);
        }
    }

    /**
     * 不为null 抛出异常
     *
     * @param object 对象
     */
    public static void isNotNull(Object object) {
        isNotNull(object, ResultEnum.FAILURE);
    }

}