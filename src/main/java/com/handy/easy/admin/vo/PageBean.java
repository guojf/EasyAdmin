package com.handy.easy.admin.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 分页参数
 *
 * @author handy
 */
@Data
public class PageBean implements Serializable {

    @ApiModelProperty(value = "每页页数", example = "1")
    private Integer page = 1;
    @ApiModelProperty(value = "每页条数", example = "10")
    private Integer limit = 10;

}