package com.handy.easy.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author handy
 */
@Data
@ApiModel
public class IdsDto implements Serializable {

    @NotEmpty
    @ApiModelProperty(value = "ids", required = true)
    private List<Long> ids;

}
