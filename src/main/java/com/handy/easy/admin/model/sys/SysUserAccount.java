package com.handy.easy.admin.model.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.handy.easy.admin.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户账号
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user_account")
public class SysUserAccount extends BaseModel {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 来源
     */
    @TableField(value = "`source`")
    private String source;

    /**
     * 一般为第三方平台的用户ID
     */
    @TableField(value = "uuid")
    private String uuid;

    /**
     * 用户名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 邮箱
     */
    @TableField(value = "email")
    private String email;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    private String password;

    public static final String COL_ID = "id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_SOURCE = "source";

    public static final String COL_UUID = "uuid";

    public static final String COL_USERNAME = "username";

    public static final String COL_EMAIL = "email";

    public static final String COL_PASSWORD = "password";

    public static final String COL_DELETED_FLAG = "deleted_flag";

    public static final String COL_CREATE_UID = "create_uid";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_UID = "update_uid";

    public static final String COL_UPDATE_TIME = "update_time";
}