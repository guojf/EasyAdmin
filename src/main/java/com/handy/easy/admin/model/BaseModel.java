package com.handy.easy.admin.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 父类
 *
 * @author handy
 */
@Data
public class BaseModel implements Serializable {

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 逻辑删除
     */
    @TableLogic
    @TableField(value = "deleted_flag", fill = FieldFill.INSERT)
    private Boolean deletedFlag;

    /**
     * 创建者
     */
    @TableField(value = "create_uid", fill = FieldFill.INSERT)
    private Long createUid;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后修改者
     */
    @TableField(value = "update_uid", fill = FieldFill.UPDATE)
    private Long updateUid;

    /**
     * 最后修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;
}
