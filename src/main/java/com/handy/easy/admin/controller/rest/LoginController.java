package com.handy.easy.admin.controller.rest;

import cn.dev33.satoken.stp.StpUtil;
import com.fujieid.jap.core.context.JapAuthentication;
import com.fujieid.jap.core.result.JapResponse;
import com.fujieid.jap.spring.boot.starter.JapTemplate;
import com.handy.easy.admin.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录接口
 *
 * @author handy
 */
@Api(tags = "登录")
@RestController
@RequiredArgsConstructor
public class LoginController {
    private final JapTemplate japTemplate;

    /**
     * 账户密码登录接口
     *
     * @return 登录结果
     */
    @ApiOperation(value = "账户密码登录接口")
    @GetMapping("/simple/login")
    public JapResponse login() {
        return japTemplate.opsForSimple().authenticate();
    }

    /**
     * 退出登录
     */
    @ApiOperation(value = "退出登录")
    @GetMapping("/logout")
    public ResultVo<String> logout(HttpServletRequest request, HttpServletResponse response) {
        JapAuthentication.logout(request, response);
        StpUtil.logout();
        return ResultVo.success("退出登录成功");
    }

}