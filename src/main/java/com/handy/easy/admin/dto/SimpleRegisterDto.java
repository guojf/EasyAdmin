package com.handy.easy.admin.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户密码注册
 *
 * @author handy
 */
@ApiModel(value = "用户密码注册")
@Data
public class SimpleRegisterDto {

    @NotBlank
    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @NotBlank
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @NotBlank
    @ApiModelProperty(value = "确定密码", required = true)
    private String twoPassword;

}