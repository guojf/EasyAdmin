package com.handy.easy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.easy.admin.model.sys.SysUserAccount;

import java.util.List;

/**
 * @author handy
 */
public interface SysUserAccountMapper extends BaseMapper<SysUserAccount> {

}