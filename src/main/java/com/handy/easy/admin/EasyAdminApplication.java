package com.handy.easy.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主类
 *
 * @author handy
 */
@SpringBootApplication
@MapperScan("com.handy.easy.admin.mapper")
public class EasyAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyAdminApplication.class, args);
    }

}