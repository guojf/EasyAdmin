package com.handy.easy.admin.model.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.handy.easy.admin.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role")
public class SysRole extends BaseModel {
    private static final long serialVersionUID = 1L;
    /**
     * 中文名称
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 权限
     */
    @TableField(value = "permission")
    private String permission;

    /**
     * 排序
     */
    @TableField(value = "sort")
    private Integer sort;

    /**
     * 描述
     */
    @TableField(value = "note")
    private String note;

    public static final String COL_ID = "id";

    public static final String COL_NAME = "name";

    public static final String COL_PERMISSION = "permission";

    public static final String COL_SORT = "sort";

    public static final String COL_NOTE = "note";

    public static final String COL_DELETED_FLAG = "deleted_flag";

    public static final String COL_CREATE_UID = "create_uid";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_UID = "update_uid";

    public static final String COL_UPDATE_TIME = "update_time";
}