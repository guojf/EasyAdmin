package com.handy.easy.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.easy.admin.model.sys.SysMenu;
import com.handy.easy.admin.model.sys.SysPermission;
import com.handy.easy.admin.model.sys.SysRole;

import java.util.List;

/**
 * @author handy
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 根据userId获取角色
     *
     * @param userId 用户id
     * @return SysRole
     */
    List<SysRole> findRoleByUserId(Long userId);

    /**
     * 根据userId获取权限
     *
     * @param userId 用户id
     * @return SysPermission
     */
    List<SysPermission> findPermissionByUserId(Long userId);

    /**
     * 根据userId获取菜单
     *
     * @param userId 用户id
     * @return SysMenu
     */
    List<SysMenu> findMenuByUserId(Long userId);

    /**
     * 根据 userId 删除角色相关数据
     *
     * @param userIds userIds
     * @return true
     */
    boolean delByUserIds(List<Long> userIds);
}