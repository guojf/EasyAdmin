layui.use('layer', function () {
    var $ = layui.jquery, layer = layui.layer;

    // 验证码
    function captcha() {
        $.ajax({
            type: "GET",
            url: "/captcha/img",
            success: function (rst) {
                if (rst.code === "1") {
                    const data = rst.data;
                    $("#captchaImg").attr('src', 'data:image/png;base64,' + data.imageBase64);
                    $("#captchaCode").text(data.captchaCode);
                }
            }
        });
    }

    // 刷新验证码
    $(document).on('click', '#captchaImg', function () {
        captcha();
    });

    // 获取验证码
    captcha();
});