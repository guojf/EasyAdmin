package com.handy.easy.admin.controller.rest;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.easy.admin.mapper.SysMenuMapper;
import com.handy.easy.admin.model.sys.SysMenu;
import com.handy.easy.admin.service.SysRoleService;
import com.handy.easy.admin.vo.init.HomeInfo;
import com.handy.easy.admin.vo.init.Init;
import com.handy.easy.admin.vo.init.LogoInfo;
import com.handy.easy.admin.vo.init.MenuInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 初始化
 *
 * @author handy
 */
@Api(tags = "初始化")
@ApiSupport(author = "handy")
@RestController
@RequiredArgsConstructor
public class InitController {
    private final SysRoleService sysRoleService;
    private final SysMenuMapper sysMenuMapper;

    @GetMapping("/init")
    @ApiOperation(value = "首页初始化参数")
    public Init init() {
        Init init = new Init();
        // 首页加载
        getHomeInfo(init);
        // logo加载
        getLogoInfo(init);
        // 菜单加载
        getMenuInfo(init);
        return init;
    }

    private void getHomeInfo(Init init) {
        HomeInfo homeInfo = new HomeInfo();
        homeInfo.setTitle("首页");
        homeInfo.setHref("/entry/welcome");
        init.setHomeInfo(homeInfo);
    }

    private void getLogoInfo(Init init) {
        LogoInfo logoInfo = new LogoInfo();
        logoInfo.setTitle("EasyAdmin");
        logoInfo.setImage("images/logo.png");
        logoInfo.setHref("");
        init.setLogoInfo(logoInfo);
    }

    private void getMenuInfo(Init init) {
        List<SysMenu> sysMenus = sysRoleService.findMenuByUserId(StpUtil.getLoginIdAsLong());
        if (CollUtil.isEmpty(sysMenus)) {
            return;
        }
        List<Tree<Long>> tree = getTree(sysMenus);
        if (CollUtil.isEmpty(tree)) {
            return;
        }
        List<MenuInfo> menuInfoList = new ArrayList<>();
        this.addMenuInfo(menuInfoList, tree);
        init.setMenuInfo(menuInfoList);
    }

    /**
     * 递归设置菜单
     *
     * @param menuInfoList 菜单列表
     * @param tree         权限树
     */
    private List<MenuInfo> addMenuInfo(List<MenuInfo> menuInfoList, List<Tree<Long>> tree) {
        for (Tree<Long> longTree : tree) {
            MenuInfo menuInfo = new MenuInfo();
            menuInfo.setTitle(longTree.getName() != null ? longTree.getName().toString() : "");
            menuInfo.setIcon(longTree.get("icon") != null ? longTree.get("icon").toString() : "");
            menuInfo.setHref(longTree.get("href") != null ? longTree.get("href").toString() : "");
            menuInfo.setTarget(longTree.get("target") != null ? longTree.get("target").toString() : "");
            List<Tree<Long>> children = longTree.getChildren();
            if (CollUtil.isNotEmpty(children)) {
                menuInfo.setChild(this.addMenuInfo(new ArrayList<>(), children));
            }
            menuInfoList.add(menuInfo);
        }
        return menuInfoList;
    }

    /**
     * 构建 菜单树
     *
     * @param sysMenus 菜单列表
     * @return 权限树
     */
    public List<Tree<Long>> getTree(List<SysMenu> sysMenus) {
        List<TreeNode<Long>> nodeList = CollUtil.newArrayList();
        for (SysMenu sysMenu : sysMenus) {
            TreeNode<Long> longTreeNode = new TreeNode<>(sysMenu.getId(), sysMenu.getParentId(), sysMenu.getTitle(), sysMenu.getSort());
            Map<String, Object> extra = new HashMap<>();
            extra.put("icon", sysMenu.getIcon());
            extra.put("href", sysMenu.getHref());
            extra.put("target", sysMenu.getTarget());
            longTreeNode.setExtra(extra);
            nodeList.add(longTreeNode);
        }
        return TreeUtil.build(nodeList, 0L);
    }

}