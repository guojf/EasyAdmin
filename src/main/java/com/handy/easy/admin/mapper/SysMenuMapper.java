package com.handy.easy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.easy.admin.model.sys.SysMenu;

public interface SysMenuMapper extends BaseMapper<SysMenu> {
}