package com.handy.easy.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 页面跳转
 *
 * @author handy
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/entry/welcome")
    public String entryHome() {
        return "entry/welcome";
    }

    @GetMapping("/entry/login")
    public String entryLogin() {
        return "entry/login";
    }

    @GetMapping("/page/table/add")
    public String pageTableAdd() {
        return "page/table/add";
    }

    @GetMapping("/page/table/edit")
    public String pageTableEdit() {
        return "page/table/edit";
    }

    @GetMapping("/page/404")
    public String page404() {
        return "page/404";
    }

    @GetMapping("/page/area")
    public String pageArea() {
        return "page/area";
    }

    @GetMapping("/page/button")
    public String pageButton() {
        return "page/button";
    }

    @GetMapping("/page/color-select")
    public String pageColorSelect() {
        return "page/color-select";
    }

    @GetMapping("/page/editor")
    public String pageEditor() {
        return "page/editor";
    }

    @GetMapping("/page/form")
    public String pageForm() {
        return "page/form";
    }

    @GetMapping("/page/form-step")
    public String pageFormStep() {
        return "page/form-step";
    }

    @GetMapping("/page/icon")
    public String pageIcon() {
        return "page/icon";
    }

    @GetMapping("/page/icon-picker")
    public String pageIconPicker() {
        return "page/icon-picker";
    }

    @GetMapping("/page/layer")
    public String pageLayer() {
        return "page/layer";
    }

    @GetMapping("/page/login-1")
    public String pageLogin1() {
        return "page/login-1";
    }

    @GetMapping("/page/login-2")
    public String pageLogin2() {
        return "page/login-2";
    }

    @GetMapping("/page/login-3")
    public String pageLogin3() {
        return "page/login-3";
    }

    @GetMapping("/page/menu")
    public String pageMenu() {
        return "page/menu";
    }

    @GetMapping("/page/setting")
    public String pageSetting() {
        return "page/setting";
    }

    @GetMapping("/page/table")
    public String pageTable() {
        return "page/table";
    }

    @GetMapping("/page/table-select")
    public String pageTableSelect() {
        return "page/table-select";
    }

    @GetMapping("/page/upload")
    public String pageUpload() {
        return "page/upload";
    }

    @GetMapping("/page/user-password")
    public String pageUserPassword() {
        return "page/user-password";
    }

    @GetMapping("/page/user-setting")
    public String pageUserSetting() {
        return "page/user-setting";
    }

    @GetMapping("/page/welcome-1")
    public String pageWelcome1() {
        return "page/welcome-1";
    }

    @GetMapping("/page/welcome-2")
    public String pageWelcome2() {
        return "page/welcome-2";
    }

    @GetMapping("/page/welcome-3")
    public String pageWelcome3() {
        return "page/welcome-3";
    }

    @GetMapping("/admin/users/user/list")
    public String adminUsersUserList() {
        return "admin/users/user/list";
    }

    @GetMapping("/admin/users/user/add")
    public String adminUsersUserAdd() {
        return "admin/users/user/add";
    }

    @GetMapping("/admin/users/user/edit")
    public String adminUsersUserEdit() {
        return "admin/users/user/edit";
    }
}
