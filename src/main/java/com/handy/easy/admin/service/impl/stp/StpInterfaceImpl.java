package com.handy.easy.admin.service.impl.stp;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.collection.CollUtil;
import com.handy.easy.admin.model.sys.SysPermission;
import com.handy.easy.admin.model.sys.SysRole;
import com.handy.easy.admin.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义权限验证接口扩展
 *
 * @author handy
 */
@Service
@RequiredArgsConstructor
public class StpInterfaceImpl implements StpInterface {
    private final SysRoleService sysRoleService;

    /**
     * 返回一个账号所拥有的权限码集合
     *
     * @param loginId   登录id
     * @param loginType 登录类型
     * @return 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<SysPermission> sysPermissions = sysRoleService.findPermissionByUserId((Long) loginId);
        if (CollUtil.isEmpty(sysPermissions)) {
            return new ArrayList<>();
        }
        return sysPermissions.stream().map(SysPermission::getPermission).collect(Collectors.toList());
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     *
     * @param loginId   登录id
     * @param loginType 登录类型
     * @return 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<SysRole> sysRoles = sysRoleService.findRoleByUserId((Long) loginId);
        if (CollUtil.isEmpty(sysRoles)) {
            return new ArrayList<>();
        }
        return sysRoles.stream().map(SysRole::getPermission).collect(Collectors.toList());
    }

}