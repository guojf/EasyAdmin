package com.handy.easy.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.handy.easy.admin.mapper.*;
import com.handy.easy.admin.model.sys.*;
import com.handy.easy.admin.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author handy
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    private final SysUserRoleMapper sysUserRoleMapper;
    private final SysPermissionMapper sysPermissionMapper;
    private final SysRolePermissionMapper sysRolePermissionMapper;
    private final SysMenuMapper sysMenuMapper;
    private final SysRoleMenuMapper sysRoleMenuMapper;

    /**
     * 根据userId获取角色
     *
     * @param userId 用户id
     * @return SysRole
     */
    @Override
    public List<SysRole> findRoleByUserId(Long userId) {
        if (userId == null) {
            return new ArrayList<>();
        }
        List<SysUserRole> sysUserRoles = sysUserRoleMapper.selectList(Wrappers.lambdaQuery(new SysUserRole())
                .eq(SysUserRole::getUserId, userId));
        if (CollUtil.isEmpty(sysUserRoles)) {
            return new ArrayList<>();
        }
        List<Long> roleIds = sysUserRoles.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        return this.listByIds(roleIds);
    }

    /**
     * 根据userId获取权限
     *
     * @param userId 用户id
     * @return SysPermission
     */
    @Override
    public List<SysPermission> findPermissionByUserId(Long userId) {
        List<SysRole> sysRoles = this.findRoleByUserId(userId);
        if (CollUtil.isEmpty(sysRoles)) {
            return new ArrayList<>();
        }
        List<Long> roleIds = sysRoles.stream().map(SysRole::getId).collect(Collectors.toList());
        List<SysRolePermission> sysRolePermissions = sysRolePermissionMapper.selectList(Wrappers.lambdaQuery(new SysRolePermission())
                .in(SysRolePermission::getRoleId, roleIds));
        if (CollUtil.isEmpty(sysRolePermissions)) {
            return new ArrayList<>();
        }
        List<Long> permissionIds = sysRolePermissions.stream().map(SysRolePermission::getPermissionId).collect(Collectors.toList());
        return sysPermissionMapper.selectBatchIds(permissionIds);
    }

    /**
     * 根据userId获取菜单
     *
     * @param userId 用户id
     * @return SysMenu
     */
    @Override
    public List<SysMenu> findMenuByUserId(Long userId) {
        List<SysRole> sysRoles = this.findRoleByUserId(userId);
        if (CollUtil.isEmpty(sysRoles)) {
            return new ArrayList<>();
        }
        List<Long> roleIds = sysRoles.stream().map(SysRole::getId).collect(Collectors.toList());
        List<SysRoleMenu> sysRoleMenus = sysRoleMenuMapper.selectList(Wrappers.lambdaQuery(new SysRoleMenu())
                .in(SysRoleMenu::getRoleId, roleIds));
        if (CollUtil.isEmpty(sysRoleMenus)) {
            return new ArrayList<>();
        }
        List<Long> menuIds = sysRoleMenus.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        return sysMenuMapper.selectBatchIds(menuIds);
    }

    /**
     * 根据 userId 删除角色相关数据
     *
     * @param userIds userIds
     * @return true
     */
    @Override
    public boolean delByUserIds(List<Long> userIds) {
        if (CollUtil.isEmpty(userIds)) {
            return true;
        }
        sysUserRoleMapper.delete(Wrappers.lambdaQuery(new SysUserRole()).in(SysUserRole::getUserId, userIds));
        return true;
    }

}