package com.handy.easy.admin.constants.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录来源
 *
 * @author handy
 */
@Getter
@AllArgsConstructor
public enum LoginSourceEnum {

    /**
     * 来源
     */
    ACCOUNT("account"),
    EMAIL("email"),
    PHONE("phone"),
    QQ("qq"),
    WX("wx"),
    ;
    private final String source;

}
