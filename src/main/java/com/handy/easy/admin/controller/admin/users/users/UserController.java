package com.handy.easy.admin.controller.admin.users.users;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.easy.admin.dto.IdsDto;
import com.handy.easy.admin.dto.users.SysUserAddDto;
import com.handy.easy.admin.dto.users.SysUserDto;
import com.handy.easy.admin.dto.users.SysUserEditDto;
import com.handy.easy.admin.dto.users.SysUserLockDto;
import com.handy.easy.admin.service.SysUserService;
import com.handy.easy.admin.vo.PageVo;
import com.handy.easy.admin.vo.ResultVo;
import com.handy.easy.admin.vo.admin.users.SysUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author handy
 */
@Api(tags = "平台用户")
@ApiSupport(author = "handy")
@RestController
@RequestMapping("/api/admin/users/user")
@RequiredArgsConstructor
public class UserController {
    private final SysUserService sysUserService;

    @GetMapping("/list")
    @ApiOperation(value = "平台用户分页列表")
    public PageVo<SysUserVo> list(@Valid SysUserDto sysUserDto) {
        return sysUserService.page(sysUserDto);
    }


    @GetMapping("/view/{id}")
    @ApiOperation(value = "用户详情")
    public ResultVo<SysUserVo> view(@PathVariable("id") Long id) {
        return ResultVo.success(sysUserService.view(id));
    }

    @PostMapping("/lock")
    @ApiOperation(value = "用户锁定状态更改")
    public ResultVo<String> lock(@RequestBody @Valid SysUserLockDto sysUserLockDto) {
        boolean rst = sysUserService.lock(sysUserLockDto);
        return rst ? ResultVo.success("更新状态成功") : ResultVo.success("更新状态失败");
    }

    @PostMapping("/add")
    @ApiOperation(value = "用户新增")
    public ResultVo<String> add(@RequestBody @Valid SysUserAddDto sysUserAddDto) {
        boolean rst = sysUserService.add(sysUserAddDto);
        return rst ? ResultVo.success("新增成功") : ResultVo.success("新增失败");
    }

    @PostMapping("/edit")
    @ApiOperation(value = "用户编辑")
    public ResultVo<String> edit(@RequestBody @Valid SysUserEditDto sysUserEditDto) {
        boolean rst = sysUserService.edit(sysUserEditDto);
        return rst ? ResultVo.success("更新成功") : ResultVo.success("更新失败");
    }

    @PostMapping("/del")
    @ApiOperation(value = "用户删除")
    public ResultVo<String> del(@RequestBody @Valid IdsDto idsDto) {
        sysUserService.del(idsDto.getIds());
        return ResultVo.success("删除成功");
    }

}