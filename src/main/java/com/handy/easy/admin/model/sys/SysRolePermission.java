package com.handy.easy.admin.model.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.handy.easy.admin.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色权限
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role_permission")
public class SysRolePermission extends BaseModel {
    /**
     * 角色ID
     */
    @TableField(value = "role_id")
    private Long roleId;

    /**
     * 权限ID
     */
    @TableField(value = "permission_id")
    private Long permissionId;

    public static final String COL_ID = "id";

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_PERMISSION_ID = "permission_id";

    public static final String COL_DELETED_FLAG = "deleted_flag";

    public static final String COL_CREATE_UID = "create_uid";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_UID = "update_uid";

    public static final String COL_UPDATE_TIME = "update_time";
}