package com.handy.easy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.easy.admin.model.sys.SysPermission;

public interface SysPermissionMapper extends BaseMapper<SysPermission> {
}