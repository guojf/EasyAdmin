package com.handy.easy.admin.vo.init;

import lombok.Data;

import java.util.List;

/**
 * 菜单
 *
 * @author handy
 */
@Data
public class MenuInfo {

    private String title;

    private String icon;

    private String href;

    private String target;

    private List<MenuInfo> child;

}
