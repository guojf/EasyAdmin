package com.handy.easy.admin.vo.admin.users;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 平台用户信息
 *
 * @author handy
 */
@Data
@ApiModel(value = "平台用户信息vo")
public class SysUserVo implements Serializable {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "用户编码")
    private String code;

    @ApiModelProperty(value = "昵称")
    private String name;

    @ApiModelProperty(value = "性别-1:男;0:女")
    private Integer sex;

    @ApiModelProperty(value = "性别")
    private String sexStr;

    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @ApiModelProperty(value = "出生日期")
    private Date birthData;

    @ApiModelProperty(value = "头像")
    private String logo;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "是否锁定（0:未锁定;1:已锁定）")
    private Boolean isLocked;

    @ApiModelProperty(value = "描述")
    private String note;

}