package com.handy.easy.admin.vo.init;

import lombok.Data;

import java.util.List;

/**
 * 初始化
 *
 * @author handy
 */
@Data
public class Init {
    private HomeInfo homeInfo;
    private LogoInfo logoInfo;
    private List<MenuInfo> menuInfo;
}
