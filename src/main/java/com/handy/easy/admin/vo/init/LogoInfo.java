package com.handy.easy.admin.vo.init;

import lombok.Data;

/**
 * logo信息
 *
 * @author handy
 */
@Data
public class LogoInfo {
    private String title;
    private String image;
    private String href;
}