package com.handy.easy.admin.constants.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * 返回码
 *
 * @author handy
 */
@Getter
@AllArgsConstructor
public enum ResultEnum implements Serializable {
    /**
     * 返回码列表
     */
    SUCCESS("1", "成功"),
    FAILURE("0", "服务器开小差了,请稍后在试"),

    PARAM_FAILURE("A00001", "参数异常"),
    NOT_DATA_FAILURE("A00002", "没有找到可操作的数据"),

    REGISTER_DUPLICATE_NAME("B00001", "注册失败: 用户名重复"),
    REGISTER_DUPLICATE_PASSWORD("B00002", "注册失败: 俩次输入密码不一致"),

    TOKEN_FAILURE("C00001", "TOKEN 错误"),
    NOT_PERMISSION("C00002", "没有操作权限"),
    NOT_ROLE("C00003", "没有角色权限"),
    ;

    private final String code;
    private final String msg;

}
