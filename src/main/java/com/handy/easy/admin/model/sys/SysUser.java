package com.handy.easy.admin.model.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.handy.easy.admin.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 系统用户
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user")
public class SysUser extends BaseModel {
    private static final long serialVersionUID = 1L;
    /**
     * 用户编码
     */
    @TableField(value = "code")
    private String code;

    /**
     * 昵称
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 性别-1:男;0:女
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 出生日期
     */
    @TableField(value = "birth_data")
    private Date birthData;

    /**
     * 头像
     */
    @TableField(value = "logo")
    private String logo;

    /**
     * 电子邮箱
     */
    @TableField(value = "email")
    private String email;

    /**
     * 是否锁定（0:未锁定;1:已锁定）
     */
    @TableField(value = "lock_flag")
    private Boolean lockFlag;

    /**
     * 描述
     */
    @TableField(value = "note")
    private String note;

    public static final String COL_ID = "id";

    public static final String COL_CODE = "code";

    public static final String COL_NAME = "name";

    public static final String COL_SEX = "sex";

    public static final String COL_BIRTH_DATA = "birth_data";

    public static final String COL_LOGO = "logo";

    public static final String COL_EMAIL = "email";

    public static final String COL_LOCK_FLAG = "lock_flag";

    public static final String COL_NOTE = "note";

    public static final String COL_DELETED_FLAG = "deleted_flag";

    public static final String COL_CREATE_UID = "create_uid";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_UID = "update_uid";

    public static final String COL_UPDATE_TIME = "update_time";
}