package com.handy.easy.admin.vo.init;

import lombok.Data;

/**
 * 首页信息
 *
 * @author handy
 */
@Data
public class HomeInfo {

    private String title;

    private String href;

}