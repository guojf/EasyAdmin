package com.handy.easy.admin.dto.users;

import cn.hutool.core.date.DatePattern;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author handy
 */
@Data
@ApiModel
public class SysUserEditDto implements Serializable {

    @NotNull
    @ApiModelProperty(value = "ID", required = true)
    private Long id;

    @NotBlank
    @ApiModelProperty(value = "昵称", required = true)
    private String name;

    @ApiModelProperty(value = "性别-1:男;0:女")
    private Integer sex;

    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    @ApiModelProperty(value = "出生日期")
    private Date birthData;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "描述")
    private String note;

}