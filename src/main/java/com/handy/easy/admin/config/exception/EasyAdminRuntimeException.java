package com.handy.easy.admin.config.exception;

import com.handy.easy.admin.constants.enums.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义运行异常
 *
 * @author handy
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EasyAdminRuntimeException extends RuntimeException {

    private ResultEnum resultEnum;

    public EasyAdminRuntimeException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.resultEnum = resultEnum;
    }

    public EasyAdminRuntimeException(String msg) {
        super(msg);
    }

    public EasyAdminRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }

}