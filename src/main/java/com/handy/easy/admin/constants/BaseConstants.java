package com.handy.easy.admin.constants;


/**
 * 常量池
 *
 * @author handy
 */
public abstract class BaseConstants {
    /**
     * 已删除标识
     */
    public static final boolean DELETE_FLAG = true;

    /**
     * 未删除标识
     */
    public static final boolean NOT_DELETE_FLAG = false;

    /**
     * 密码加盐值
     */
    public static final String SALT = "Easy-Admin";

}