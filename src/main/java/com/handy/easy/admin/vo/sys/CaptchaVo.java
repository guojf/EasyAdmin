package com.handy.easy.admin.vo.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 验证码参数
 *
 * @author handy
 */
@ApiModel
@Data
public class CaptchaVo {
    @ApiModelProperty(value = "验证码")
    private String captchaCode;

    @ApiModelProperty(value = "验证码base64")
    private String imageBase64;

}
