package com.handy.easy.admin.config.jap;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

/**
 * jap 过滤配置
 *
 * @author handy
 */
@Configuration
@RequiredArgsConstructor
public class JapConfig implements WebMvcConfigurer {
    private final JapInterceptor japInterceptor;
    /**
     * 过滤的路径
     * 登录,退出登录
     * oauth 协议登录,oidc 协议登录,第三方登录,账号密码登录
     * 静态资源,icon
     * swagger相关
     * openapi
     */
    private final static List<String> FILTER_URL_LIST = Arrays.asList(
            "/entry/login", "/logout", "/register/**", "/captcha/**",
            "/oauth2/**", "/oidc/**", "/social/**", "/simple/**",
            "/lib/**", "/js/**", "/images/**", "/css/**", "/api/**","/mods/**", "/favicon.ico",
            "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**", "/doc.html/**", "/swagger-resources", "/error",
            "/openapi/**");

    /**
     * 配置路径
     *
     * @param registry registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册jap的路由拦截器
        registry.addInterceptor(japInterceptor)
                .excludePathPatterns(FILTER_URL_LIST)
                .addPathPatterns("/**");
    }

}