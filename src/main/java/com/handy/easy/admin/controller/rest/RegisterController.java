package com.handy.easy.admin.controller.rest;

import com.handy.easy.admin.dto.SimpleRegisterDto;
import com.handy.easy.admin.service.SysUserService;
import com.handy.easy.admin.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册
 *
 * @author handy
 */
@Api(tags = "注册")
@RestController
@RequestMapping("/register")
@RequiredArgsConstructor
public class RegisterController {
    private final SysUserService sysUserService;

    /**
     * 用户密码注册
     *
     * @return 登录结果
     */
    @ApiOperation(value = "用户密码注册")
    @PostMapping("/simple")
    public ResultVo<Boolean> simpleRegister(@RequestBody @Validated SimpleRegisterDto simpleRegisterDto) {
        return sysUserService.simpleRegister(simpleRegisterDto);
    }

}
