package com.handy.easy.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.handy.easy.admin.model.sys.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}