package com.handy.easy.admin.dto.users;

import com.handy.easy.admin.vo.PageBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 平台用户信息
 *
 * @author handy
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "平台用户信息Dto")
public class SysUserDto extends PageBean {

    @ApiModelProperty(value = "用户编码")
    private String code;

    @ApiModelProperty(value = "昵称")
    private String name;

}