package com.handy.easy.admin.model.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.handy.easy.admin.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 权限资源
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_permission")
public class SysPermission extends BaseModel {
    /**
     * 权限
     */
    @TableField(value = "permission")
    private String permission;

    /**
     * 访问路径
     */
    @TableField(value = "url")
    private String url;

    /**
     * 名称
     */
    @TableField(value = "`name`")
    private String name;

    public static final String COL_ID = "id";

    public static final String COL_PERMISSION = "permission";

    public static final String COL_URL = "url";

    public static final String COL_NAME = "name";

    public static final String COL_DELETED_FLAG = "deleted_flag";

    public static final String COL_CREATE_UID = "create_uid";

    public static final String COL_CREATE_TIME = "create_time";

    public static final String COL_UPDATE_UID = "update_uid";

    public static final String COL_UPDATE_TIME = "update_time";
}