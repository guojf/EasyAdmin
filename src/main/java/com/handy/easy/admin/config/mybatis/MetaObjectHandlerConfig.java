package com.handy.easy.admin.config.mybatis;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.handy.easy.admin.constants.BaseConstants;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * 配置字段默认规则
 *
 * @author handy
 */
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // 默认未删除
        setFieldValByName("deletedFlag", BaseConstants.NOT_DELETE_FLAG, metaObject);
        // 创建人
        setFieldValByName("createUid", StpUtil.isLogin() ? StpUtil.getLoginIdAsLong() : 0L, metaObject);
        // 创建时间默认当前时间
        setFieldValByName("createTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 修改人
        setFieldValByName("updateUid", StpUtil.isLogin() ? StpUtil.getLoginIdAsLong() : 0L, metaObject);
        // 修改时间
        setFieldValByName("updateTime", new Date(), metaObject);
    }

}