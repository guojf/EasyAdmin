package com.handy.easy.admin.dto.users;

import com.handy.easy.admin.vo.PageBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * 平台用户信息
 *
 * @author handy
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "平台用户信息Dto")
public class SysUserLockDto extends PageBean {

    @NotNull
    @ApiModelProperty(value = "用户ID", required = true)
    private Long id;

    @NotNull
    @ApiModelProperty(value = "是否锁定", required = true)
    private Boolean lockFlag;

}