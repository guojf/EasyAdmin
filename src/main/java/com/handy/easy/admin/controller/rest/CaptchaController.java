package com.handy.easy.admin.controller.rest;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.generator.RandomGenerator;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.handy.easy.admin.vo.ResultVo;
import com.handy.easy.admin.vo.sys.CaptchaVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 验证码
 *
 * @author handy
 */
@Api(tags = "验证码")
@ApiSupport(author = "handy")
@RestController
@RequestMapping("/captcha")
@RequiredArgsConstructor
public class CaptchaController {

    @GetMapping("/img")
    @ApiOperation(value = "获取图片验证码")
    public ResultVo<CaptchaVo> getImg() {
        // 自定义纯数字的验证码（随机4位数字，可重复）
        RandomGenerator randomGenerator = new RandomGenerator("0123456789", 4);
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 38);
        lineCaptcha.setGenerator(randomGenerator);
        // 生成code
        lineCaptcha.createCode();
        // 返回值
        CaptchaVo captchaVo = new CaptchaVo();
        captchaVo.setCaptchaCode(lineCaptcha.getCode());
        captchaVo.setImageBase64(lineCaptcha.getImageBase64());
        return ResultVo.success(captchaVo);
    }

}
