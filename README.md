# 项目说明
一款基于springboot的后台管理快速开发模块化脚手架  

# 目前代码分支分为:
1. master分支,项目为单一架构,方便springboot一键启动

# 团队成员
[handy](http://gitee.com/handy-git)

# 演示地址
[点击前往演示地址](https://easy.ljxmc.top)    
帐号:root 密码:123456, 请不要删除该账户或者变更锁定状态
swagger接口管理的密码为admin/admin 请善待开源公共资源

# 目前功能说明
-  **用户中心**
    1. 平台用户
    2. 验证码查询
    3. 登录日志
-  **系统设置**
    1. 角色权限
    2. 菜单管理
    3. 接口管理
    4. 定时任务

# 技术
|  类型   |   名称  |   版本  |  链接   |
| ---  | --- | --- | --- |
|  java     |  jdk   |  1.8   |     |
|  核心框架 |   spring boot  |  2.5.6  |  [spring boot官网](https://spring.io/projects/spring-boot/ )  |
|  视图框架|   spring mvc  |     |  |
|  持久层框架   |   mybatis-plus  |  3.4.3.4   |   [mybatis-plus官网](https://mp.baomidou.com/)  |
|  接口文档   |  knife4j   |  3.0.3  |     |
|  模板引擎   |   thymeleaf  |    |     |
|  前端页面   |   layui|  2.6.3   |  [layui官网](https://www.layui.com/)   |
|  前端页面   |   layuimini v2|   |  [layuimini链接](https://gitee.com/zhongshaofa/layuimini)   |
|  数据库   |   mysql|  5.7   |     |
|  工具  |   lombok|     |     |
|  工具包|   hutool|  5.7.16  |   |

### 阿里云代金券
- 本项目大量使用阿里云产品:演示版的 服务器,数据库,oss储存,短信服务,如果想自己搭建起来的话可以领取下面代金券优惠购买
- 建议注册个 新账户购买 ，享受2折 ，如果购买多个产品，可以先放到 购物车 再一键购买
- [阿里云代金券2000元,点击即可获取](https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=zdax7tgo)

### 腾讯云双十一活动，新老用户可买3台
[腾讯云双十一活动](https://cloud.tencent.com/act/double11?spread_hash_key=116c4f37b7525b27fde7cdd6421fcf0a)

# 部署说明
EasyAdmin 是使用Maven构建的springboot项目

一. 导入数据库
1. 执行 table.sql 文件创建表
2. 执行 init.sql 初始化数据

二. 修改相关配置
修改application.properties里的数据库配置

三. 启动项目
1. idea:本项目为Springboot项目启动,直接启动EasyAdminApplication类即可
2. 直接使用maven把该项目打包成jar,使用java -jar EasyAdmin.jar启动

# 备注
- 如果您有疑问,你可以评论回复。
- 如果EasyAdmin对您有一点帮助，您可以点个star，就是对作者最大的支持了。
- EasyAdmin会继续更新下去，努力成为一款快速上手又非常好用的脚手架。

# 贡献代码的步骤
1. 在Gitee上fork项目到自己的repo
2. 把fork过去的项目也就是你的项目clone到你的本地
3. 修改代码
4. commit后push到自己的库
5. 登录Gitee在你首页可以看到一个 pull request 按钮，点击它，填写一些说明信息，然后提交即可。
6. 等待维护者合并

# 效果图
![登录](https://images.gitee.com/uploads/images/2021/1120/180906_e3dab367_1604115.png "屏幕截图.png")
![首页](https://images.gitee.com/uploads/images/2021/1120/180922_7fd738f9_1604115.png "屏幕截图.png")
![平台用户](https://images.gitee.com/uploads/images/2021/1120/180933_61108946_1604115.png "屏幕截图.png")
![平台用户新增](https://images.gitee.com/uploads/images/2021/1120/180947_d2e7e93a_1604115.png "屏幕截图.png")
![常规管理-菜单管理](https://images.gitee.com/uploads/images/2021/1120/181019_cfbeeee4_1604115.png "屏幕截图.png")
![组件管理-图标选择](https://images.gitee.com/uploads/images/2021/1120/181039_d7d83d91_1604115.png "屏幕截图.png")
![其他管理-按钮](https://images.gitee.com/uploads/images/2021/1120/181100_b4189b85_1604115.png "屏幕截图.png")