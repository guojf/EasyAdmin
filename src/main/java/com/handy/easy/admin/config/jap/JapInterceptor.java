package com.handy.easy.admin.config.jap;

import cn.dev33.satoken.stp.StpUtil;
import com.fujieid.jap.core.context.JapAuthentication;
import com.fujieid.jap.core.result.JapResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截器
 *
 * @author handy
 * @date 2021/11/11 16:38
 */
@Slf4j
@Component
public class JapInterceptor implements HandlerInterceptor {

    /**
     * 前置拦截
     *
     * @param request  request
     * @param response response
     * @param handler  handler
     * @return true/false
     * @throws Exception Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info(request.getRequestURI());
        JapResponse japResponse = JapAuthentication.checkUser(request, response);
        // jap 和 sa-token 同时校验
        if (japResponse.isSuccess() && StpUtil.isLogin()) {
            return true;
        }
        // 未登录重定向的路径
        response.sendRedirect("/entry/login");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}