package com.handy.easy.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.handy.easy.admin.dto.SimpleRegisterDto;
import com.handy.easy.admin.dto.users.SysUserAddDto;
import com.handy.easy.admin.dto.users.SysUserDto;
import com.handy.easy.admin.dto.users.SysUserEditDto;
import com.handy.easy.admin.dto.users.SysUserLockDto;
import com.handy.easy.admin.model.sys.SysUser;
import com.handy.easy.admin.vo.PageVo;
import com.handy.easy.admin.vo.ResultVo;
import com.handy.easy.admin.vo.admin.users.SysUserVo;

import java.util.List;

/**
 * @author handy
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 用户密码注册
     *
     * @param simpleRegisterDto 入参
     * @return 结果
     */
    ResultVo<Boolean> simpleRegister(SimpleRegisterDto simpleRegisterDto);

    /**
     * 分页查询用户列表
     *
     * @param sysUserDto 入参
     * @return SysUserVo
     */
    PageVo<SysUserVo> page(SysUserDto sysUserDto);

    /**
     * 用户详情
     *
     * @param id userId
     * @return SysUserVo
     */
    SysUserVo view(Long id);

    /**
     * 锁定
     *
     * @param sysUserLockDto 入参
     * @return true
     */
    boolean lock(SysUserLockDto sysUserLockDto);

    /**
     * 新增
     *
     * @param sysUserAddDto 入参
     * @return true
     */
    boolean add(SysUserAddDto sysUserAddDto);

    /**
     * 编辑
     *
     * @param sysUserEditDto 入参
     * @return true
     */
    boolean edit(SysUserEditDto sysUserEditDto);

    /**
     * 删除
     *
     * @param ids 入参
     */
    void del(List<Long> ids);

}